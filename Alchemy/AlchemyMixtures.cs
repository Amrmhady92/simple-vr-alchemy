﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kandooz.Sausage
{


    [CreateAssetMenu(fileName = "AlchemyMixtures", menuName = "Alchemy", order = 0)]
    public class AlchemyMixtures : ScriptableObject
    {
        [System.Serializable]
        private class AlchemyMixture
        {
            public string mixtureName;
            public AlchemyOutput alchemyOutputPrefab;
            public List<string> componentNames;
            public bool needCorrectOrder = false;
        }

        [SerializeField]
        private List<AlchemyMixture> mixtures;

        public AlchemyOutput GetAlchemyOutput(List<string> componentsNames)
        {
            AlchemyOutput output = null;
            if (mixtures == null || mixtures.Count == 0)
            {
                Debug.LogError("AlchemyMixture Object "+this.name+" has no Mixtures");
                return output;
            }

            

            for (int i = 0; i < mixtures.Count; i++)
            {
                if (mixtures[i].componentNames == null ||
                    mixtures[i].alchemyOutputPrefab == null ||
                    mixtures[i].componentNames.Count == 0 || 
                    mixtures[i].componentNames.Count != componentsNames.Count)
                {


                    if (mixtures[i].alchemyOutputPrefab == null) Debug.Log("Mixture " + i + " has no prefab referenced");
                    else if (mixtures[i].componentNames == null || mixtures[i].componentNames.Count == 0) Debug.Log("Mixture " + i + " has no components List");
                    else if (mixtures[i].componentNames.Count != componentsNames.Count) Debug.Log("Not Mixture of output " + mixtures[i].alchemyOutputPrefab.OutputName);
                                      
                    continue;
                }

                List<string> givenNames = new List<string>(componentsNames);

                if (mixtures[i].needCorrectOrder == false)
                {
                    mixtures[i].componentNames.Sort();
                    givenNames.Sort();
                }

                bool found = true;

                for (int x = 0; x < givenNames.Count; x++)
                {
                    found = found && givenNames[x] == mixtures[i].componentNames[x];
                    if (!found) break;
                }
                if (!found) continue;

                output = mixtures[i].alchemyOutputPrefab;
                Debug.Log("Found Mixture : "+ output.OutputName);
                break;
            }

            return output;
        }
    }
}
