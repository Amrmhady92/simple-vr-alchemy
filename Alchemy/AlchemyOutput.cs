﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using Kandooz.KVR;

namespace Kandooz.Sausage
{
    public enum MixtureType
    {
        Filled,
        Surfacing
    }

    public enum AlchemyFlaskType
    {
        Flask,
        Pouch,
        Object
    }

    public class AlchemyOutput : MonoBehaviour
    {
        [SerializeField]
        private string outputName;

        public string OutputName
        {
            get
            {
                return outputName;
            }

            private set
            {
                outputName = value;
            }
        }
        public MixtureType mixtureOutputType = MixtureType.Filled;
        public AlchemyFlaskType alchemyFlaskTypeRequired;

        [Header("After Potion Used the Following should Happen")]
        public UnityEvent OnPotionDrank;


        private InteractableGrabbable myGrabbable;

        private void Start()
        {
            myGrabbable = this.GetComponent<InteractableGrabbable>();
        }
    }
}
