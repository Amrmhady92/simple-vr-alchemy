﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kandooz.KVR;

namespace Kandooz.Sausage
{
    [RequireComponent(typeof(InteractableGrabbable))]
    public class AlchemyComponent : MonoBehaviour
    {
        public string componentName;
        public Color liquidColorChanger;
        [SerializeField] private bool canDuplicate = false;

        public Transform originalParentPosition;
        private bool mixable = true;
        private bool firstGrab = true;
        private bool restoring = false;
        private Vector3 originalScale;
        private Rigidbody rb;
        InteractableGrabbable myGrabbable;

        public bool CanDuplicate
        {
            get
            {
                return canDuplicate;
            }

            private set
            {
                canDuplicate = value;
            }
        }

        private void Start()
        {
            myGrabbable = this.GetComponent<InteractableGrabbable>();
            myGrabbable.OnGrab.AddListener(OnGrabbed);
            originalScale = this.transform.localScale;
            rb = this.GetComponent<Rigidbody>();

            if(originalParentPosition == null)
            {
                var newParent = new GameObject(this.name + "Transform");
                newParent.transform.parent = null;
                newParent.transform.position = this.transform.position;
                newParent.transform.rotation = this.transform.rotation;
                this.transform.parent = newParent.transform;
                originalParentPosition = newParent.transform;
            }
        }
        List<bool> meshRendsStates;
        private void OnTriggerEnter(Collider other)
        {
            AlchemyStation station = other.GetComponent<AlchemyStation>();
            if(station != null && mixable)
            {
                station.AddComponentToStation(this);
                MeshRenderer meshrend = this.GetComponent<MeshRenderer>();
                if(meshrend != null) meshrend.enabled = (false);
                else
                {
                    var meshRends = this.GetComponentsInChildren<MeshRenderer>();
                    meshRendsStates = new List<bool>();
                    if(meshRends != null && meshRends.Length > 0)
                    {
                        for (int i = 0; i < meshRends.Length; i++)
                        {
                            meshRendsStates.Add(meshRends[i].enabled);
                            meshRends[i].enabled = false;
                        }
                    }
                }

                myGrabbable.UnGrabMe();
                //Play Particle Effect here
                StartCoroutine(Refresh());

            }

            if (other.CompareTag("Bounds") && firstGrab && !myGrabbable.InUse)
            {
                RestoreToPosition();
            }

        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Bounds") && firstGrab && !myGrabbable.InUse)
            {
                RestoreToPosition();
            }
        }

        private IEnumerator Refresh()
        {
 

            myGrabbable.Grabbable = false;
            mixable = false;

            rb.isKinematic = true;
            this.transform.position = new Vector3(10000, 10000, 10000);

            yield return new WaitForSeconds(3);
            //this.GetComponent<MeshRenderer>().enabled = (true);
            MeshRenderer meshrend = this.GetComponent<MeshRenderer>();
            if (meshrend != null) meshrend.enabled = (true);
            else
            {
                var meshRends = this.GetComponentsInChildren<MeshRenderer>();
                if (meshRends != null && meshRends.Length > 0)
                {
                    for (int i = 0; i < meshRends.Length; i++)
                    {
                        meshRends[i].enabled = meshRendsStates[i];
                    }
                }
            }

            this.transform.parent = originalParentPosition;
            this.transform.localPosition = Vector3.zero;
            this.transform.localEulerAngles = Vector3.zero;
            this.transform.localScale = Vector3.zero;
            rb.isKinematic = false;
            LeanTween.scale(this.gameObject, originalScale, 1).setOnComplete(()=> { myGrabbable.Grabbable = true; mixable = true; });
            //yield return new WaitForSeconds(1);
            //myGrabbable.Grabbable = true;
            //mixable = true;
        }


        List<bool> triggerBool;
        private void RestoreToPosition()
        {
            if (restoring) return;
            restoring = true;
            mixable = false;
            rb.useGravity = false;

            rb.velocity = Vector3.zero;
            this.transform.parent = originalParentPosition;

            Collider[] colliders = this.GetComponents<Collider>();
            triggerBool = new List<bool>();
            for (int i = 0; i < colliders.Length; i++)
            {
                //colliders[i].enabled = false;
                triggerBool.Add(colliders[i].isTrigger);
                colliders[i].isTrigger = false;
            }

            LeanTween.rotateLocal(this.gameObject, Vector3.zero, 2);
            LeanTween.moveLocal(this.gameObject, Vector3.zero, 2).setOnComplete(()=>
            {

                rb.useGravity = true;
                mixable = true;
                restoring = false;
                for (int i = 0; i < colliders.Length; i++)
                {
                    colliders[i].isTrigger = triggerBool[i];
                }
            });
        }

        private void CancelRestore()
        {
            if (!restoring) return;
            LeanTween.cancelAll();
            Collider[] colliders = this.GetComponents<Collider>();
            for (int i = 0; i < colliders.Length; i++)
            {
                colliders[i].isTrigger = triggerBool[i];
            }
            restoring = false;
            mixable = true;
            rb.useGravity = true;
        }

        public void OnGrabbed()
        {
            firstGrab = true;
            myGrabbable.OnGrab.RemoveListener(OnGrabbed);
        }

    }
}