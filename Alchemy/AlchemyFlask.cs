﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kandooz.KVR;

namespace Kandooz.Sausage
{
    
    [RequireComponent(typeof(InteractableFarInteraction))]
    [RequireComponent(typeof(InteractableGrabbable))]
    public class AlchemyFlask : MonoBehaviour
    {
        public GameObject emptyFlaskObject;
        public GameObject dirtyFlaskObject;
        public Transform filledFlaskTransform;


        private InteractableFarInteraction farInteraction;
        private AlchemyOutput currentOutput;
        private GameObject createdPotion;
        private bool firstGrab = false;
        private bool filled;
        private Vector3 newFlaskStartPosition = Vector3.zero;
        private Vector3 newFlaskStartERotiation = Vector3.zero;
        private Transform originalParent;

        public AlchemyOutput CurrentOutput
        {
            get
            {
                return currentOutput;
            }

            private set
            {
                currentOutput = value;
                if(currentOutput != null)
                {
                    if (createdPotion != null) Destroy(createdPotion);
                    createdPotion = Instantiate(currentOutput.gameObject, filledFlaskTransform);
                    createdPotion.transform.localPosition = Vector3.zero;
                    createdPotion.transform.localEulerAngles = Vector3.zero;
                    Filled = true;
                    emptyFlaskObject.SetActive(false);
                }
                else
                {
                    if (createdPotion != null) Destroy(createdPotion);
                    Filled = false;
                }
            }
        }

        public bool Filled
        {
            get
            {
                return filled;
            }

            private set
            {
                filled = value;
                emptyFlaskObject.SetActive(!filled);
                if (!filled)
                {
                    if (createdPotion != null) Destroy(createdPotion);
                }
            } 
        }

        private void Start()
        {
            farInteraction = this.GetComponent<InteractableFarInteraction>();
            newFlaskStartPosition = this.transform.position;
            newFlaskStartERotiation = this.transform.eulerAngles;
            originalParent = this.transform.parent;
        }
        private void OnTriggerEnter(Collider other)
        {
            if (Filled) return;
            AlchemyStation station = other.GetComponent<AlchemyStation>();
            if (station != null)
            {
                if (station.CurrentState == AlchemyStationState.Output)
                {
                    if(station.CurrentAlchemyOutput != null)
                    {                       
                            if(station.CurrentAlchemyOutput.alchemyFlaskTypeRequired == AlchemyFlaskType.Flask)
                            {
                                CurrentOutput = station.CurrentAlchemyOutput;
                            }                       
                    }
                    //CurrentOutput = station.GetOutPutFromComponentMixture();
                }
                else
                {
                    Filled = true;
                    dirtyFlaskObject.SetActive(true);
                }
            }
        }

        public void OnGrabFlask()
        {
            if (firstGrab) return;
            firstGrab = true;
            // Create New Flask
            StartCoroutine(WaitAndMakeNewFlask());
        }
        IEnumerator WaitAndMakeNewFlask()
        {
            GameObject bottle = Instantiate(this.gameObject, originalParent);
            bottle.transform.position = newFlaskStartPosition;
            bottle.transform.eulerAngles = newFlaskStartERotiation;

            Vector3 originalScale = bottle.transform.localScale;
            bottle.transform.localScale = Vector3.zero;

            yield return new WaitForSeconds(2);
           
            bottle.GetComponent<InteractableGrabbable>().Grabbable = false;
            LeanTween.scale(bottle, originalScale, 2).setOnComplete(()=> { bottle.GetComponent<InteractableGrabbable>().Grabbable = true; });

        }
    }
}