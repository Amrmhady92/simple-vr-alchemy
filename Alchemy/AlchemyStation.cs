﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using System;

namespace Kandooz.Sausage
{

    public enum AlchemyStationState
    {
        Clean,
        Mixing,
        Output,
        Cleaning,
        Dirty
    }

    public class AlchemyStation : MonoBehaviour
    {
        [SerializeField] private AlchemyMixtures alchemyMixturesFile;
        [SerializeField] private int maxComponents = 3;
        [SerializeField] private Surfacer floater;
        [SerializeField] private float forceRemoveDuration = 10;

        [SerializeField] private ParticleSystem[] cleaningParticlesObjects;
        [SerializeField] private string cleaningSaltsName = "Cleaning Salts";
        [SerializeField] private float cleaningTime = 2f;

        [SerializeField] private Transform forceRemoveTransform;


        [SerializeField] private UnityEvent onComponentAdded;
        [SerializeField] private UnityEvent onMixtureProducedClean;
        [SerializeField] private UnityEvent onMixtureProducedDirty;


        [SerializeField] private AlchemyOutput currentAlchemyOutput;
        [SerializeField] private AlchemyStationState currentState;
        [SerializeField] private List<string> componentsInserted;

       // private MeshRenderer meshRenderer;
        [SerializeField] private CauldronColorChanger cauldronColor;
        [SerializeField] private Color dirtyColor;

        private bool outputObjectInCauldron = false;

       

        public AlchemyStationState CurrentState
        {
            get
            {
                return currentState;
            }

            private set
            {
                currentState = value;
                switch (currentState)
                {
                    case AlchemyStationState.Clean:
                        //meshRenderer.material.color = cleanColor;
                        if(cauldronColor != null) cauldronColor.ResetColor();
                        StopCoroutine("OutputWaitBeforeForceRemove");
                        break;

                    case AlchemyStationState.Mixing:
                        break;

                    case AlchemyStationState.Dirty:
                        //meshRenderer.material.color = dirtyColor;
                        if (cauldronColor != null) cauldronColor.SetColor(dirtyColor);

                        break;

                    case AlchemyStationState.Cleaning:
                        // Set State to Cleaning
                        // Reset List
                        // Play Bubbles Particles
                        // Change Color to White
                        // Wait and after a point and set State to Clean

                        if (cauldronColor != null) cauldronColor.SetColor(Color.white);
                        //if (cleaningParticlesObject != null) cleaningParticlesObject.SetActive(true);
                        for (int i = 0; i < cleaningParticlesObjects.Length; i++)
                        {
                            if(cleaningParticlesObjects[i] == null)
                            {
                                Debug.Log("Null Particle");
                                continue;
                            }

                            var emm = cleaningParticlesObjects[i].emission;
                            emm.enabled = true;
                        }

                        StartCoroutine(CleanAfter());

                        break;
                    default:
                        break;
                }
            }
        }

        public AlchemyOutput CurrentAlchemyOutput
        {
            get
            {
                return currentAlchemyOutput;
            }

            private set
            {
                currentAlchemyOutput = value;

                if(currentAlchemyOutput != null)
                {
                    if(currentAlchemyOutput.mixtureOutputType == MixtureType.Surfacing)
                    {
                        if(floater != null)
                        {
                            currentAlchemyOutput = GameObject.Instantiate(currentAlchemyOutput.gameObject, floater.transform).GetComponent<AlchemyOutput>();
                            currentAlchemyOutput.transform.localPosition = Vector3.zero;
                            currentAlchemyOutput.transform.localEulerAngles = Vector3.zero;
                            floater.StartSurfacing(currentAlchemyOutput.gameObject);
                            //var obj = GameObject.Instantiate(currentAlchemyOutput.gameObject, floater.transform);
                            //obj.transform.localPosition = Vector3.zero;
                            //obj.transform.localEulerAngles = Vector3.zero;
                            //floater.StartSurfacing(obj);
                            StartCoroutine(OutputWaitBeforeForceRemove());
                        }
                    } 
                }
            }
        }

        private IEnumerator OutputWaitBeforeForceRemove()
        {
            Debug.Log("Destroying after " + forceRemoveDuration);
            yield return new WaitForSeconds(forceRemoveDuration);
            //CleanStation();
            CurrentState = AlchemyStationState.Cleaning;
        }

        private bool ForceRemoveOutput()
        {
            bool success = false;
            if (CurrentAlchemyOutput != null)
            {
                Debug.Log("Not Null , Destroying");
                if (CurrentAlchemyOutput.transform.parent == floater.transform)
                {
                    Debug.Log("Still In");

                    if (forceRemoveTransform != null)
                    {
                        CurrentAlchemyOutput.transform.parent = forceRemoveTransform;
                        Rigidbody rb = CurrentAlchemyOutput.gameObject.GetComponent<Rigidbody>();
                        if (rb) rb.isKinematic = true;
                        LeanTween.move(CurrentAlchemyOutput.gameObject, forceRemoveTransform, 2f);
                        CurrentState = AlchemyStationState.Clean;
                        success = true;
                    }
                    else
                    {
                        GameObject obj = CurrentAlchemyOutput.gameObject;
                        Debug.Log("No Force Remove Transform found, Destroying Object");
                        CurrentAlchemyOutput.transform.parent = null;
                        LeanTween.scale(obj, Vector3.zero, 1f).setOnComplete(() =>
                        {
                            Destroy(obj);
                            CurrentState = AlchemyStationState.Clean;
                            success = true;
                        });
                    }
                }
                else
                {
                    Debug.Log("not In\n CurrentAlchemyOutput.transform.parent : "+ CurrentAlchemyOutput.transform.parent+ "\nfloater.transform :"+ floater.transform);
                }
            }
            if (floater) floater.StopSurfacing();

            return success;
        }

        void Start()
        {
            componentsInserted = new List<string>();
            //meshRenderer= this.GetComponent<MeshRenderer>();
            if (cauldronColor == null)
            {
                cauldronColor = this.GetComponentInParent<CauldronColorChanger>();
            }
            if (cauldronColor == null) Debug.Log("No Color Changer Found/Referenced");
            

        }



        // Component Added to the Station on the Max Components added the Mixture is evaluated
        public void AddComponentToStation(AlchemyComponent component)
        {

            if(CurrentState == AlchemyStationState.Output)
            {
                Debug.Log("Can't Mix Components , Remove Object");
                return;
            }

            if (CurrentState == AlchemyStationState.Cleaning)
            {
                Debug.Log("Can't Mix Components , Cauldron is Being cleaned");
                return;
            }

            if (component.componentName.ToLower() == cleaningSaltsName.ToLower())
            {              
                CurrentState = AlchemyStationState.Cleaning;
                return;
            }


            Debug.Log("Initiate Alchemy Component "+ component.name + " Add");
            if (CheckComponent(component) == false) return;
            

            if(CurrentState != AlchemyStationState.Dirty)
            {
                Debug.Log("Alchemy Component " + component.name + " Added");
                componentsInserted.Add(component.componentName);
                if (cauldronColor != null) cauldronColor.SetColor(component.liquidColorChanger);
                onComponentAdded?.Invoke();
            }
            else
            {
                Debug.Log("Station is Dirty now, Clean");
                return;
            }



            if (componentsInserted.Count < maxComponents)
            {
                CurrentState = AlchemyStationState.Mixing;
            }
            else
            if (componentsInserted.Count == maxComponents)
            {
                Debug.Log("Components Max Reached, Mixing");

                CurrentAlchemyOutput = alchemyMixturesFile.GetAlchemyOutput(componentsInserted);

                if (CurrentAlchemyOutput == null)
                {
                    Debug.Log("CurrentAlchemyOutput = null");
                    CurrentState = AlchemyStationState.Dirty;
                    onMixtureProducedDirty?.Invoke();
                }
                else
                {
                    Debug.Log("CurrentAlchemyOutput Name = "+ CurrentAlchemyOutput.OutputName);
                    CurrentState = AlchemyStationState.Output;
                    onMixtureProducedClean?.Invoke();
                }
            }
            else CurrentState = AlchemyStationState.Dirty;
            

        }

        /// <summary>
        /// Checks the integrity of the Component, if It is Null , if the Name is valid , and if it was a duplicate when it cant be inserted
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        private bool CheckComponent(AlchemyComponent component)
        {
            //If Component wasnt destroyed somehow or if null
            if (component == null)
            {
                Debug.LogError("Component Is NULL, nothing Happened");
                return false;
            }

            // If there is no Name or Null
            if (string.IsNullOrEmpty(component.componentName))
            {
                Debug.Log("Component Has No Name, Add Name to it, nothing Has happened");
                return false;
            }

            //Check if it cant be a duplicate , if so check if already inserted
            // Used mainly for Drops from Bottles etc.
            if (component.CanDuplicate == false && componentsInserted.Contains(component.componentName))
            {
                Debug.Log("Component Cant Duplicate and Already Inserted, nothing Has happened");
                return false;
            }

            //Everything is fine, now back with true
            return true;
        }

        public void CleanStation()
        {
            if (!ForceRemoveOutput()) { CurrentState = AlchemyStationState.Clean; };
            CurrentAlchemyOutput = null;
            componentsInserted.Clear();
        }

        private IEnumerator CleanAfter()
        {
            yield return new WaitForSeconds(cleaningTime);
            CleanStation();
            for (int i = 0; i < cleaningParticlesObjects.Length; i++)
            {
                if (cleaningParticlesObjects[i] == null)
                {
                    Debug.Log("Null Particle");
                    continue;
                }

                var emm = cleaningParticlesObjects[i].emission;
                emm.enabled = false;
            }
        }
        private void OnMouseDown()
        {
            CleanStation();
        }

    }
}
